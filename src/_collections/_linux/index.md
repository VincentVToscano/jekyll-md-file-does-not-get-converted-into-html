---
title: &title All Linux Learning Topics Including Articles, How-To Articles, and Snippets
headline: Explore the World of Linux-based Operating Systems
subheadline: Linux Distributions Education
permalink: /linux/
meta:
    title: *title
    description: &description Linux 
    keywords: &keywords linux, linus, Linux, distributions, distros, ubuntu, CentOS, RHEL-based, RHEL, debian, red hat linux, Red Hat Enterprise, Fedora-based, fedora, arch linux, Raspbian, bsd, mandriva linux, Linux Mint Debian Edition, SteamOS, Xubuntu, Ubuntu Server, Ubuntu MATE, Ubuntu GNOME, GNOME, Ubuntu Mobile, Chromium OS, Gentoo-based, Gentoo, RPM-based, RPM, openSUSE, Viperr, ServOS, Aurora, Oracle Linux, Slackware-based, Slackware, Slackware ARM, Porteus, KNOPPIX, Salix, FreeNAS
    og:
        title: *title
        description: *description
        site_name: site_name
        type: article
        image: imageBla
        image_width: image_width
        image_height: image_height
        published_time: 2017-03-14
        modified_time: &moded 2018-01-04
        updated_time: *moded
        tag: *keywords
        rich_attachment: true
sitemap:
  lastmod: *moded
  priority: 0.7
  changefreq: monthly
  exclude: no
nav_label: Linux
nav_actual: 0
nav_sub: 10
---
**This page provides links to articles and How-To's regarding Linux distributions (or distros), package management systems, DevOps-related tasks, installations and configurations. The information provided herein may relate to Debian-based, Ubuntu-based, Gentoo-based, Pacman-based, Arch Linux-based, RPM-based, Fedora-based, RHEL-based, Mandriva Linux-based, openSUSE-based, Slackware-based, Slax-based, Independent, and Third-party distributions.

------------

{:markdown=1}
##### Popular Linux Articles:
<ul markdown="1">
	{% for item in site.linux limit:33 %}
	{% if item.url != '/linux/' and item.url != '/linux/articles/' and item.url != '/linux/how-to/' and item.url != '/linux/snippets/' %}
	<li><a href="{{ item.url | append:' ' | replace:'/ ','' }}">{{ item.headline }}</a></li>
	{% endif %}
	{% endfor %}
</ul>
<br>

<br>
<section data-category="articles" markdown="1">  
##### Articles:
 <ul>
     <li><a href="/{{page.collection}}/articles" title="View all {{page.collection | capitalize}} articles">All {{page.collection | capitalize}} Articles</a></li>
 </ul>

</section>

<section data-category="how-tos" markdown="1">
##### How-to Articles:
<ul>
    <li><a href="/{{page.collection}}/how-to" title="View all {{page.collection | capitalize}} how-to topics">All {{page.collection | capitalize}} How-To Articles</a>  </li>
</ul>

</section>

<section data-category="snippets" markdown="1">
##### Snippets:
<ul>
    <li><a href="/{{page.collection}}/snippets" title="View all {{page.collection | capitalize}} snippets">All {{page.collection | capitalize}} Snippets</a></li>
</ul>

</section>
---
title: &title Get Octal File Permissions from Command-line on Linux and BSD
headline: Get Octal File Permissions from Command-line
subheadline: Use the stat with various options to get file and directory permissions. 
meta:
    context: TechArticle
    title: *title
    description: &description Use stat to get Linux file permissions as well as directory permissions.
    keywords: &keywords stat, file permissions, directory permissions folder permissions, octal permissions, user privileges, user rights on Linux  
    og:
        title: *title
        description: *description
        site_name: site_name
        type: article
        image: imageBla
        image_width: image_width
        image_height: image_height
        published_time: 2018-02-22
        modified_time: &moded 2018-02-22
        updated_time: *moded
        tag: *keywords
        rich_attachment: true
sitemap:
  lastmod: *moded
  priority: 0.7
  changefreq: monthly
  exclude: no
---
Quickly get file permissions and directory permissions on Unix-like operating systems such as Linux, BSD, macOS, OS X, Debian, Ubuntu, RHEL, CentOS, Fedora and more using _stat_.
```bash
stat -c '%A %a %n' ~/LinuxCommands/mkdir
```  
The output:  
```bash
drwxrwxr-x 775 LinuxCommands
```  

Format sequences explained:  

+ %a = The access rights in octal
+ %A = The access rights in human readable form or format
+ %n = The file name  

<br>
Type **man stat** on command-line to get all available format sequences for your operating system.

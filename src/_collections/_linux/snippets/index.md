---
title: &title All Linux Learning Snippets
headline: Linux Snippets
subheadline: View Unix shell and command language plus associated programming language code snippets
permalink: /linux/snippets/
meta:
    title: *title
    description: &description Linux code snippets 
    keywords: &keywords linux Snippets, code snippets, bash, code, bash code, bash tidbits, quick linux answers
    og:
        title: *title
        description: *description
        site_name: site_name
        type: article
        image: imageBla
        image_width: image_width
        image_height: image_height
        published_time: 2017-03-14
        modified_time: &moded 2018-01-03
        updated_time: *moded
        tag: *keywords
        rich_attachment: true
sitemap:
  lastmod: *moded
  priority: 0.7
  changefreq: monthly
  exclude: no
nav_label: Linux Code Snippets
nav_actual: 10
nav_sub: na
---
#### Snippets (TBD):
<ul>
	{% for page in site.linux limit:33 %}
	{% if item.url contains 'snippets' and item.url != '/linux/' and item.url != '/linux/how-to/' %}
	<li><a href="{{ item.url }}">{{ item.headline }}</a></li>
	{% endif %}
	{% endfor %}
</ul>
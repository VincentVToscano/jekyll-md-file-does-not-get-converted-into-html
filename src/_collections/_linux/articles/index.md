---
title: &title Linux Learning Articles - Home
headline: &headline Linux Learning Articles - Home
subheadline: Learn new subjects or revisit past subjects.
permalink: /linux/articles/
meta:
    title: *title
    description: &description Linux Articles
    keywords: &keywords linux articles
    og:
        title: *title
        description: *description
        site_name: site_name
        type: article
        image: imageBla
        image_width: image_width
        image_height: image_height
        published_time: 2018-01-02
        modified_time: &moded 2018-01-04
        updated_time: *moded
        tag: *keywords
        rich_attachment: true
sitemap:
  lastmod: *moded
  priority: 0.7
  changefreq: weekly
  exclude: no
nav_label: Linux Articles
nav_actual: 10
nav_sub: 11
---
Below is a collection of articles pertaining to Linux topics written by Techkraft contributors. In the future, this list will expand to include additional topics spanning all things Linux. Please check back weekly.

------------
#### Articles Covering Linux Commands:
<ul>
	{% for item in site.linux limit:33 %}
        {% if item.url contains 'commands' and item.url != '/linux/' and item.url != '/linux/articles/' %}
        {% assign cmd = item.url | remove:'/index.html' | split: '/' | last %}
        	<li><a href="{{ item.url | append:' ' | replace:'/ ','' }}">{%if cmd != 'commands' %}{{ cmd }}{% else %}All Linux Commands - Home{% endif %}</a></li>
        {% endif %}
	{% endfor %}
</ul>
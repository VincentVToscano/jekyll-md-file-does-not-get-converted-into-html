---
title: &title Linux Commands
headline: &headline Learn Linux Commands
subheadline: Core commands you should know, explained in detail, with accompanying examples. 
permalink: /linux/articles/commands/
meta:
    title: *title
    description: &description Linux Commands
    keywords: &keywords Linux Commands, cd, mkdir, ls, ll
    og:
        title: *title
        description: *description
        site_name: site_name
        type: article
        image: imageBla
        image_width: image_width
        image_height: image_height
        published_time: 2018-01-16
        modified_time: &moded 2018-01-16
        updated_time: *moded
        tag: *keywords
        rich_attachment: true
sitemap:
  lastmod: *moded
  priority: 0.6
  changefreq: monthly
  exclude: no
nav_label: *headline
nav_actual: 11
nav_sub: na
---

Below is a collection of articles pertaining to all Linux commands (and their command line options) Techkraft has reviewed thus far. In the future, this list will expand to include additional commands,  usages, and combinations.

The commands list includes commands that may be found as the core installs for {{site.data.linux.distrosShort}}.  

------------
#### Linux Commands:
<ul>
	{% for item in site.linux limit:33 %}
        {% if item.url contains 'commands' and item.url != '/linux/' and item.url != '/linux/articles/' and item.url != '/linux/how-to/' and item.url != '/linux/snippets/' and item.url != page.url %}
        {% assign cmd = item.url | remove:'/index.html' | split: '/' | last %}
        	<li><a href="{{ item.url | append:' ' | replace:'/ ','' }}">{%if cmd != 'commands' %}{{ cmd }}{% else %}All Linux Commands - Home{% endif %}</a></li>
        {% endif %}
	{% endfor %}
</ul>
---
title: &title Make or Create Directories
headline: The mkdir command
subheadline: Learn how to make or create new directories
meta:
    context: TechArticle
    title: *title
    description: &description The mkdir command is utilized to create or make directories.
    keywords: &keywords directory, make directory, current directory, absolute pathname, location relative, root directory, local pathname, local pathname, changing to a subdirectory
    og:
        title: *title
        description: *description
        site_name: site_name
        type: article
        image: imageBla
        image_width: image_width
        image_height: image_height
        published_time: 2017-03-14
        modified_time: &moded 2018-01-04
        updated_time: *moded
        tag: *keywords
        rich_attachment: true
sitemap:
  lastmod: *moded
  priority: 0.5
  changefreq: monthly
  exclude: no
---
Use this command to create new directories along the Linux directory tree. This is dummy data used to test the integrity of the compilation/build process.  
  
Making a new paragraph (<p>) element is easy. To do so, use markdown syntax. Separate each paragraph with a line that contains two spaces, followed by a return. Markdown requires two spaces to designate a line break.


To learn all markdown syntax, view the documentation by these resources:  
- [kramdown Syntax](https://kramdown.gettalong.org/syntax.html "Kramdown Syntax Documentation"){: .externalFacingLink .standardLink target="_blank"}  
- [GitHub Flavored Markdown](https://guides.github.com/features/mastering-markdown/ "GitHub"){: .externalFacingLink .standardLink target="_blank"}  
  
Below is an example of syntax highlighting for bash.  


```bash
/var/www/
├── VHOST1
│   ├── PROJ1
│   │   └── www
│   ├── PROJ2
│   │   └── www
│   └── PROJ3
│       └── www
├── VHOST2
│   ├── PROJ1
│   │   └── www
│   ├── PROJ2
│   │   └── www
│   └── PROJ3
│       └── www
└── VHOST3
    ├── PROJ1
    │   └── www
    ├── PROJ2
    │   └── www
    └── PROJ3
        └── www
```  
Please note that you must have permission to create directories with the path to use the mkdir otherwise the system will express that the user you’re currently logged in as (echo whoami) does not have permission. 